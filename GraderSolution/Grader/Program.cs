﻿using System;
using System.IO;
using System.Collections.Generic;
using System.Linq;

//Grader by Liam Shand

namespace Grader
{
    public class GradeSorter
    {
        public class MarkedStudent
        {
            public string FirstName { get; private set; }
            public string LastName { get; private set; }
            public int Score { get; private set; }

            public MarkedStudent(string first, string last, int score)
            {
                this.FirstName = first;
                this.LastName = last;
                this.Score = score;
            }
        }

        /// <summary>
        /// Converts a string into a MarkStudent List
        /// </summary>
        /// <param name="fileContents"></param>
        /// <returns>List<MarkedStudent></returns>
        public List<MarkedStudent> ParseStudents(string fileContents)
        {
            List<MarkedStudent> students = new List<MarkedStudent>();
            {
                string[] splitInputLines = fileContents.Split('\n');
                for (int i = 0; i < splitInputLines.Length; i++)
                {
                    string[] splitLine = splitInputLines[i].Split(' ');
                    if (splitLine.Length > 2)
                    {
                        //check if valid format; add to students list
                        string lastname = splitLine[0].ToUpper();
                        string firstname = splitLine[1].ToUpper();
                        int score;
                        if (int.TryParse(splitLine[2], out score))
                        {
                            students.Add(new MarkedStudent(firstname, lastname, score));
                        }
                    }
                }
            }
            return students;

        }

        /// <summary>
        /// Reads in file contents and sets to fileContents parameter and returns a boolean if it was successful in reading the file. 
        /// </summary>
        /// <param name="path"></param>
        /// <param name="fileContents"></param>
        /// <returns>Boolean</returns>
        public bool ReadGrades(string path, out string fileContents)
        {
            bool success = true;
            fileContents = "";
            try
            {
                using (StreamReader sr = new StreamReader(path))
                {
                    fileContents = sr.ReadToEnd();
                }

            }
            catch(FileNotFoundException)
            {
                Console.WriteLine("ExceptionError: File not found");
                success = false;
            }
            catch (Exception)
            {
                Console.WriteLine("ExceptionError: Could not read file"/* + e*/);
                success = false;
            }

            return success;
        }
        
        /// <summary>
        /// Writes MarkedStudent List content to a file at a given path and returns a boolean if it was successful in writing to the file. 
        /// </summary>
        /// <param name="students"></param>
        /// <param name="path"></param>
        /// <returns>Boolean</returns>
        public bool WriteSortedGrades(List<MarkedStudent> students, string path)
        {
            bool success = true;

            IEnumerable<MarkedStudent> query =
                            from s in students
                            orderby s.Score descending, s.LastName, s.FirstName
                            select s;
            try
            {
                using (StreamWriter sw = new StreamWriter(path))
                {
                    foreach (var s in query)
                    {
                        sw.Write(s.LastName + " " + s.FirstName + " " + s.Score + "\r\n\r\n");
                    }
                }
            }
            catch (Exception)
            {
                Console.WriteLine("ExceptionError: Could not write to file");
                success = false;
            }

            return success;
        }

        /// <summary>
        /// Reads a path pointing to a student scores file and creates a sorted version of it with "-graded" appended to the name.
        /// </summary>
        /// <param name="specifiedPath"></param>
        public void GradeSort(string specifiedPath)
        {
            if (specifiedPath != "")
            {
                string[] firstSplit = specifiedPath.Split('\\', '/');
                if (firstSplit[firstSplit.Length - 1] != "")
                {
                    string dir, fileName;
                    dir = fileName = "";
                    for (int i = 0; i < firstSplit.Length - 1; i++)
                    {
                        dir += firstSplit[i] + "\\";
                    }

                    fileName = firstSplit[firstSplit.Length - 1];
                    string[] fileNameSplit = fileName.Split('.');
                    string fileNameOnly = fileNameSplit[0];
                    string fileExtension = (fileNameSplit.Length > 1) ? fileNameSplit[1] : "";

                    if (dir == "" || Directory.Exists(dir))
                    {
                        if (File.Exists(specifiedPath))
                        {
                            if (fileExtension == "txt" || fileExtension == "")
                            {
                                string newFileName = fileNameOnly + "-graded." + "txt";
                                string path = dir + fileNameOnly + "." + fileExtension;
                                string newpath = dir + newFileName;

                                //READ FILE
                                string inputlines;
                                //true if file exists
                                if (ReadGrades(path, out inputlines))
                                {
                                    //PARSE STUDENTS
                                    List<MarkedStudent> students = ParseStudents(inputlines);

                                    //WRITE FILE
                                    if (WriteSortedGrades(students, newpath))
                                    {
                                        Console.WriteLine("Finished: Created " + newFileName);
                                    }
                                }
                            } else { Console.WriteLine("Invalid: File extension not supported; this program only accepts text files"); }
                        } else { Console.WriteLine("Invalid: File doesn't exist\nUsage: Grader <[Directories\\]FileName>"); }
                    } else { Console.WriteLine("Invalid: Directory doesn't exist\nUsage: Grader <[Directories\\]FileName>"); }
                } else { Console.WriteLine("Invalid: Must include a filename\nUsage: Grader <[Directories\\]FileName>"); }
            } else { Console.WriteLine("Usage: Grader <[Directories\\]FileName>"); }
        }

    }

    public class Program
    {

        static void Main(string[] args)
        {
            GradeSorter gs = new GradeSorter();
            if (args.Length > 0)
            {
                gs.GradeSort(args[0]);
            }
            else
            {
                Console.WriteLine("Usage: Grader <[Directories\\]FileName>");
            }

        }
    }
}
