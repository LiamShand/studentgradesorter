﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;
using System.IO;
using Grader;

//GraderTest by Liam Shand

namespace GraderTest
{
    public class GraderSorterGenericTest
    {
        public void Generic_GradeSortTester(string testpath, string expectedOutput)
        {
            using (StringWriter sw = new StringWriter())
            {
                Console.SetOut(sw);
                GradeSorter gs = new GradeSorter();
                try
                {
                    gs.GradeSort(testpath);
                }
                catch (Exception e)
                {
                    Assert.Fail("Exception was thrown: " + e);
                    return;
                }
                Assert.AreEqual<string>(expectedOutput, sw.ToString());
            }
        }
    }

    [TestClass]
    public class GradeSorterTest
    {
        //These tests are to be conducted with \InputOutFiles\ 
        //being an existing directory inside of GraderTest UnitTesting Project.
        //Existing files in that directory include:
        //-->PNGTEST.png
        //-->EmptyContent.txt
        //-->Sample.txt
        //-->StandardFile
        //Generated files:
        //-->Sample-graded.txt
        //-->EmptyContent-graded.txt
        //-->StandardFile-graded.txt
        
        [TestMethod]
        public void GradeSort_PathWithOnlyDirectory_NoException()
        {
            string testpath = @"..\..\InputOutputFiles\";
            string expected = string.Format("Invalid: Must include a filename\nUsage: Grader <[Directories\\]FileName>{0}", Environment.NewLine);

            GraderSorterGenericTest gt = new GraderSorterGenericTest();
            gt.Generic_GradeSortTester(testpath, expected);

        }

        [TestMethod]
        public void GradeSort_NOTEXIST_FileNameSharesNameAsEXISTINGTextWithNoExtension_NoException()
        {
            string testpath = @"..\..\InputOutputFiles\Sample";
            string expected = string.Format("Invalid: File doesn't exist\nUsage: Grader <[Directories\\]FileName>{0}", Environment.NewLine);

            GraderSorterGenericTest gt = new GraderSorterGenericTest();
            gt.Generic_GradeSortTester(testpath, expected);

        }

        [TestMethod]
        public void GradeSort_STANDARD_FileNameWithNoExtension_NoException()
        {
            string testpath = @"..\..\InputOutputFiles\StandardFile";
            string expected = string.Format("Finished: Created StandardFile-graded.txt{0}", Environment.NewLine);

            GraderSorterGenericTest gt = new GraderSorterGenericTest();
            gt.Generic_GradeSortTester(testpath, expected);

        }

        [TestMethod]
        public void GradeSort_FileNameWithTextExtension_NoException()
        {
            string testpath = @"..\..\InputOutputFiles\Sample.txt";
            string expected = string.Format("Finished: Created Sample-graded.txt{0}", Environment.NewLine);

            GraderSorterGenericTest gt = new GraderSorterGenericTest();
            gt.Generic_GradeSortTester(testpath, expected);

        }

        [TestMethod]
        public void GradeSort_FileNameWithPNGExtension_NoException()
        {
            string testpath = @"..\..\InputOutputFiles\PNGTEST.png";
            string expected = string.Format("Invalid: File extension not supported; this program only accepts text files{0}", Environment.NewLine);

            GraderSorterGenericTest gt = new GraderSorterGenericTest();
            gt.Generic_GradeSortTester(testpath, expected);
        }

        [TestMethod]
        public void GradeSort_NOTEXIST_NoNameWithTextExtension_NoException()
        {
            string testpath = @"..\..\InputOutputFiles\.txt";
            string expected = string.Format("Invalid: File doesn't exist\nUsage: Grader <[Directories\\]FileName>{0}", Environment.NewLine);

            GraderSorterGenericTest gt = new GraderSorterGenericTest();
            gt.Generic_GradeSortTester(testpath, expected);

        }

        [TestMethod]
        public void GradeSort_NOTEXIST_FileNameWithTextExtension_NoException()
        {
            string testpath = @"..\..\InputOutputFiles\DoesNotExist.txt";
            string expected = string.Format("Invalid: File doesn't exist\nUsage: Grader <[Directories\\]FileName>{0}", Environment.NewLine);

            GraderSorterGenericTest gt = new GraderSorterGenericTest();
            gt.Generic_GradeSortTester(testpath, expected);
        }

        [TestMethod]
        public void GradeSort_EMPTYCONTENT_FileNameWithTextExtension_NoException()
        {
            string testpath = @"..\..\InputOutputFiles\EmptyContent.txt";
            string expected = string.Format("Finished: Created EmptyContent-graded.txt{0}", Environment.NewLine);

            GraderSorterGenericTest gt = new GraderSorterGenericTest();
            gt.Generic_GradeSortTester(testpath, expected);

        }

        [TestMethod]
        public void GradeSort_AlternatingDirectorySlashesFileText_NoException()
        {
            string testpath = @"../..\InputOutputFiles/Sample.txt";
            string expected = string.Format("Finished: Created Sample-graded.txt{0}", Environment.NewLine);

            GraderSorterGenericTest gt = new GraderSorterGenericTest();
            gt.Generic_GradeSortTester(testpath, expected);

        }

        [TestMethod]
        public void GradeSort_DirectoryNOTEXIST_FileNOTEXIST_FileNameText_NoException()
        {
            string testpath = @"..\..\InputOutputFiles\DoesNotExistFolder\DoesNotExist.txt";
            string expected = string.Format("Invalid: Directory doesn't exist\nUsage: Grader <[Directories\\]FileName>{0}", Environment.NewLine);

            GraderSorterGenericTest gt = new GraderSorterGenericTest();
            gt.Generic_GradeSortTester(testpath, expected);

        }
    }
}
